### What this role does

main.yml really just calls either provision or teardown depending on activity. Provision calls in resources.yml to set up the vpc.Teardown destroys the instances and the vpc. defaults contains default values for aws requirements, IPs, region, OS and size.

### Using the role (example)

```yaml
---
- name: Provision AWS Infrastructure
  hosts: localhost
  connection: local
  gather_facts: false

  roles:
    - role: redhatautomation.aws.aws_infrastructure
      vars:
        teardown: no
```

```yaml
---
- name: Teardown
  hosts: localhost
  connection: local
  gather_facts: false

  roles:
    - role: redhatautomation.aws.aws_infrastructure
      vars:
        teardown: yes
```
